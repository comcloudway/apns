---
title: "HowTo: Devise a Device"
date: 2022-03-10T16:39:11+01:00
draft: false
tags: [ "smart-home", "how-to" ]
image: "model.png"
---

# HowTo: Devise a Device
Devising a Device can be hard,
and besides the work that has to go into designing a new device,
one also has to decide what kind of device he wants to devise. \
This is where *Mr. Deciding Device* comes to your rescue:
He is the **ultimate** gadget designed to make decisions for you.

## Making a decision

<img src = "model.png" style="float:right; max-width:35%; width: 300px" />

Apparently you do not know what kind of device you want to develop,
but *Mr. Deciding Device* is here to help you decide on one of your ideas.

Before activating him,
make sure to preload the climate controlled Full HD glass container with your ideas.
Make sure that your ideas are written on a blue sheet of paper,
because otherwise they won't seem like blueprints to *Mr. Deciding Device*
and he will feel offended. \
After you preloaded the container, you can press the decision-making button.
*Mr. Deciding Device* will now try to make a decision
and print it on the piece of paper.
Please note that this process might take some time,
and if *Mr. Deciding Device* is bored,
he will not decide,
so you have to keep talking to him until he has made his decision.

## Coming up with ideas
After discovering *Mr. Deciding Device*,
you initially feel relieved,
overwhelmed by the feeling of finally deciding which machine you want to develop,
but then you notice that you actually have no ideas of your own.

Luckily, *Mr. Deciding Device* has your back,
and after preloading it with a combination of words and letters stolen from one of Shakespeare's plays
and rerunning the previous step,
he will come up with an idea.

## Finalizing
As you can see,
the initial process of devising a device can be quite challenging. \
But there are gadgets that can help you,
and if you aren't happy with the results you got,
maybe you want to check out the article on *Mrs. Coming-Up-With-An-Idea Device*,
which we are currently working on.

Actually to be honest -
*Mr. Deciding Device* and all the other gadgets are amazing,
and work perfectly,
which is why you can get started building your invention now.
