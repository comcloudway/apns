---
title: "Welcome"
date: 2022-02-26T15:21:13+01:00
tags: [ "welcome" ]
---

Well I guess this is the first blog post.

This should probably be somewhat special - shouldn't it.
But it doesn't really feel that way.

I meant after all, these are just words,
made of letters, seperated by spaces.

But maybe in the end,
those words, 
every letter and number,
every space and line break,
form a bigger picture.

But in the end,
this won't be about the result,
instead lets see were the road takes us.
