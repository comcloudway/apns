+++
title = "Introducing: City Tree"
date = 2022-02-26T15:43:31+01:00
tags = [ "introducing", "featuring", "environment" ]
image = "banner.png"
description = "A way to safe our planet?"
+++

Most of you probably already know that air pollution can lead to illness,
as the small particles can damage part of the body,
make lung disease worse or lead to heart attacks.
That is why we have to find a solution to air pollution.
One of these solutions could be the CityTree.


# What is the CityTree?
<img src = "https://greencitysolutions.de/wp-content/uploads/2020/12/GCS_CT2020_Studio_info_update-1-e1608288550310-800x642.png" style="float:left; max-width:50%; width: 300px" />

As the CityTree is a pilot project,
and is still in active development,
most of you probably have not heard of the CityTree.

Basically, the CityTree is a three-and-a-half meter tall structure,
whose surface is covered with moss.
Additionally, it provides space to sit down and rest.

To ensure that the plants are watered frequently,
the CityTree has moisture sensors
and needs an electricity and water connection.

The CityTrees themselves are constructed in Bestensee,
south-east of Berlin,
at the same manufacturing site,
where the moss is being grown.


# Promises
<img src="https://greencitysolutions.de/wp-content/uploads/2021/02/GIF-EN-slow-min.gif" style="float:right; max-width:50%; width:300px"/>
After reading what the CityTree is made of,
you might be wondering how moss is supposed to be useful.
But you will be surprised to hear that moss is a great natural air filter,
as its large surface area can absorb many pollutants.

Still, the CityTree has not been without controversy:
For example, the company claimed that their first-generation trees were able to filter as much air as 275 real trees.
After being proven wrong, the company disregarded these claims.

Now focusing on the newest CityTree generation,
they claim that one tree can filter the air for 7 thousand adults.


# Test series
After six years of development,
three CityTrees are now being tested in Berlin at various locations,
including the zoo.

The company also plans on testing their CityTrees in schools,
stating that they want to ensure good air quality for students.
Additionally, they plan on informing them about the dangers of air pollution.

To ensure that the company sticks to its promises,
the tests are monitored by some big Institutions.


# Funding
Innovative projects are always quite expensive,
especially when they have been in development for six years.
You might remember our air pollution project group looking for sponsors,
when we were trying to do our own research.

Luckily, Green City Solutions,
the company developing the CityTree,
was able to secure 1.9 million euros in funding from the Horizon 2020 EU funds.

# Future
Supporting start-ups is always a process involving risks.
Which is why we have decided to wait for the test results,
before jumping on board and ordering a couple of CityTrees for our school.

Assuming that the trees function as advertised,
we would be happy for your support,
either financially or from a management point of view.

# Sources
- Most of the images used, and some additional information, were taken from the official [CityTree homepage](https://greencitysolutions.de/en/citytree/)
- text transcript of a radio report
broadcasted by the rbb radio station.
