---
title: "Links"
date: 2022-02-26T15:17:34+01:00
menu:
    main: 
        weight: -50
        params:
            icon: link
comments: false
---

This page is like a lonely planet - we do not reference external sites.

But if you are looking for some funny videos - you might want to search for `Foil Arms and Hog`.

If you are more into the other kind of funny, check out `James Veitch`.
