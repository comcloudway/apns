---
title: "About"
date: 2022-02-26T15:14:03+01:00
aliases:
  - about-us
  - about-hugo
  - contact
license: CC BY-NC-ND
menu:
    main: 
        weight: -90
        params:
            icon: user
---
Some of my original school work.

Published under CC BY-NC.

**CONTENT WARNING: SOME OR ALL PAGES CONTAIN FICTIONAL WORK**
